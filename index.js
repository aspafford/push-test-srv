var app = require('express')()
var server = require('http').createServer(app)
var io = require('socket.io')(server)
const bodyParser = require('body-parser')

let clientRef = {}

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.post('/api/particleUpdate', (req, res) => {

  // console.log('ping from particle', clientRef)
  // let o = {
  //   coreid: req.body.coreid,
  //   data: req.body.data
  // }

  for (let client in clientRef) {
    clientRef[client].emit('timer', req.body)
  }

  res.send('ok')

})

io.on('connection', (client) => {

  clientRef[client.id] = client

  console.log('client id:', client.id)

  client.on('subscribeToTimer', (interval) => {

    setInterval(() => {

      clientRef = client

      console.log('client ref refreshing')

    }, interval);
    
  });
});

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

server.listen(PORT, HOST);
